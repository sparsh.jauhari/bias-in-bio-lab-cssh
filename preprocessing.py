'''
This file contains all the preprocessing tasks like tokenization, stopwords removal, stemming etc. 
It also contains all the embeddings like CountVector, Word2vec and ELMo and modules to fit and transform them.
'''

import nltk
import tensorflow as tf
from nltk.stem.snowball import SnowballStemmer
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer
from config import WORD2VEC_PATH, DEBIASED_WORD2VEC_PATH, DEBIASED_SELF_WORD2VEC_PATH, DATASET_NAMES, MASKED
from gensim.models import KeyedVectors, Word2Vec
from joblib import dump, load
import numpy as np
import pandas as pd
import tensorflow_hub as hub
from config import CLASS_GROUP
from sampling import get_data_from_mongo, get_distinct_field_values
from sklearn.svm import SVC
from config import DATASET_NAMES, MASKED, SEED
import numpy as np
 
# nltk.download('punkt')
# nltk.download('stopwords')
stop_words=set(nltk.corpus.stopwords.words("english"))



# Generic embeddings fit and transform module
def embedding_fit_transform(x_list, embedding, class_group, sampling, test_size, masking):
    print("processing preprocessing.embedding_fit_transform ...")
    
    if embedding == 'cv': 
        trained_embedding, X = count_vectorize_fit_transform(x_list,masking)
        dump(trained_embedding,DATASET_NAMES['embedding',embedding,class_group,sampling,test_size,MASKED[masking]]+'.joblib')
        print("\t saving file :",DATASET_NAMES['embedding',embedding,class_group,sampling,test_size,MASKED[masking]])
    
    elif embedding == 'self_w2v':
        trained_embedding, X = selftrained_word2vec_fit_transform(x_list,masking)
        dump(trained_embedding,DATASET_NAMES['embedding','self_w2v',MASKED[masking]]+'.joblib')
        print("\t saving file :",DATASET_NAMES['embedding','self_w2v',MASKED[masking]])
    
    elif embedding == 'w2v':
        X = pretrained_word2vec_transform(x_list,masking)
    
    elif embedding == 'elmo':
        X = elmo_transform(x_list,masking)  

    return(X)



# Generic embedding transform module
def embedding_transform(x_list, embedding, class_group, sampling, test_size, masking ):
    print("processing preprocessing.embedding_transform ...")
    
    if embedding == 'cv': 
        trained_embedding = load(DATASET_NAMES['embedding',embedding,class_group,sampling,test_size,MASKED[masking]]+'.joblib')
        X = count_vectorize_transform(trained_embedding,x_list)
    
    elif embedding == 'self_w2v':
        trained_embedding = load(DATASET_NAMES['embedding','self_w2v',MASKED[masking]]+'.joblib')
        X = word2vec_transform(trained_embedding,x_list,masking)
    
    elif embedding == 'w2v':
        X = pretrained_word2vec_transform(x_list,masking)
        
    elif embedding == 'd_w2v':
        X = pretrained_debiased_word2vec_transform(x_list,masking)
    
    elif embedding == 'd_self_w2v':
        X = selftrained_debiased_word2vec_transform(x_list,masking)
    
    elif embedding == 'elmo':
        X = elmo_transform(x_list,masking)
    return(X)


''' Add this with every embedding
    -> RegexpTokenizer
    -> nltk english stop_words
    -> SnowballStemmer'''
def preProcessStopAndTokenize(sentence):
    tokenizer = nltk.RegexpTokenizer(r"\w+")
    tokenized_words=tokenizer.tokenize(sentence.lower())
    filtered_words=[]
    for w in tokenized_words:
       if w.lower() not in stop_words:
           filtered_words.append(w)
    snowball_stemmer = SnowballStemmer('english', ignore_stopwords=True)   
    stemmed = [snowball_stemmer.stem(word) for word in filtered_words]
    return stemmed


''' -> RegexpTokenizer
    -> SnowballStemmer'''
def preProcessAndTokenize(sentence):
    tokenizer = nltk.RegexpTokenizer(r"\w+")
    tokenized_words=tokenizer.tokenize(sentence.lower())
    snowball_stemmer = SnowballStemmer('english')   
    stemmed = [snowball_stemmer.stem(word) for word in tokenized_words]
    return stemmed



# CountVectorizer
def count_vectorize_fit_transform(x_list,masking):
    if masking:
        vectorizer_binary = CountVectorizer(lowercase=True, preprocessor=None, binary=True, stop_words=None, tokenizer=preProcessStopAndTokenize)
    else:
        vectorizer_binary = CountVectorizer(lowercase=True, preprocessor=None, binary=True, stop_words=None, tokenizer=preProcessAndTokenize)

    X = vectorizer_binary.fit_transform(x_list)
    print("count vectorized with dimension : ",len(vectorizer_binary.get_feature_names()))
    return vectorizer_binary, X


def count_vectorize_transform(vectorizer_binary,x_list):
    X = vectorizer_binary.transform(x_list)
    return X



# Word2Vec
def word2vec_transform(model, x_list,masking):
    if masking:
        X = np.array([np.mean([model[word] for word in filter(lambda x: x in model,preProcessStopAndTokenize(sent))],axis=0) for sent in x_list])    
    else:
        X = np.array([np.mean([model[word] for word in filter(lambda x: x in model,preProcessAndTokenize(sent))],axis=0) for sent in x_list])    
    return X


def pretrained_word2vec_transform(x_list,masking):
    model = KeyedVectors.load_word2vec_format(WORD2VEC_PATH, binary = True)
    X = word2vec_transform(model, x_list,masking)
    return X


def selftrained_word2vec_fit(corpus,masking):
    if masking:
        tokensed_corpus = [preProcessStopAndTokenize(sent) for sent in corpus]
    else:
        tokensed_corpus = [preProcessAndTokenize(sent) for sent in corpus]
    model = Word2Vec(tokensed_corpus, vector_size = 100).wv
    return model


def selftrained_word2vec_fit_transform(x_list,masking):
    corpus = get_distinct_field_values(MASKED[masking])
    model = selftrained_word2vec_fit(corpus,masking)
    X = word2vec_transform(model, x_list,masking)
    return model , X



# Debiased Word2Vec
def pretrained_debiased_word2vec_transform(x_list,masking):
    model = KeyedVectors.load_word2vec_format(DEBIASED_WORD2VEC_PATH, binary = True)
    X = word2vec_transform(model, x_list,masking)
    return X
  

def selftrained_debiased_word2vec_transform(x_list,masking):
    model = KeyedVectors.load_word2vec_format(DEBIASED_SELF_WORD2VEC_PATH, binary = True)
    X = word2vec_transform(model, x_list,masking)
    return X   



# ELMo
def elmo_transform(x_list,masking):
    elmo = hub.load("https://tfhub.dev/google/elmo/3")
    X = []   
    for sent in x_list:
        if masking:
            sent = preProcessStopAndTokenize(sent)
        else:
            sent = preProcessAndTokenize(sent)
        sent = ' '.join(sent)     
        embeddings = elmo.signatures["default"](tf.constant([sent]))    
        X.append(np.mean(embeddings['word_emb'],1).flatten()) 
    return X





