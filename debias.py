'''
This file is runnable and contains modules for debiasing the Word2Vec embeddings. 
The following is the refactored version of the code that can be found on: https://github.com/tolga-b/debiaswe/blob/10277b23e187ee4bd2b6872b507163ef4198686b/debiaswe/we.py."""
The code is supposed to be run at level: ~/bias-in-bio-lab-cssh/word_embeddings.
'''

from joblib import dump, load
import numpy as np
import json 
from sklearn.decomposition import PCA
from gensim.models import KeyedVectors, Word2Vec
unicode = str
""" CLASSES represents the medical domain which we aim to debias."""
CLASSES = ['physician',
            'nurse',
            'psychologist',
            'dentist',
            'surgeon',
            'dietitian',
            'chiropractor'
        ]
    
""" embd__tv____r.joblib is word2vec model trained on our medical domain."""
model = load('bias-in-bio-lab-cssh/word_embeddings/embd__tv____r.joblib') 
vecs =[]
words = [w for w in model.index_to_key ]    
vecs = [model[w] for w in words]
vecs = np.array(vecs, dtype='float32')
index = {w: i for i, w in enumerate(words)}

   
def normalize(vecs):
        """Normalizes the vectors."""
        vecs /= np.linalg.norm(vecs, axis=1)[:, np.newaxis]
               
with open('bias-in-bio-lab-cssh/word_embeddings/definitional_pairs.json', "r") as f:
        """The ten pairs of words used to define the gender direction.
        The file can be found at: https://github.com/tolga-b/debiaswe/tree/10277b23e187ee4bd2b6872b507163ef4198686b/data"""
        definitional_pairs = json.load(f)


with open('bias-in-bio-lab-cssh/word_embeddings/gender_specific_full.json', "r") as f:
        """  A list of 1441 gender-specific words.
        The file can be found at:  https://github.com/tolga-b/debiaswe/tree/10277b23e187ee4bd2b6872b507163ef4198686b/data"""
        gender_specific_words = json.load(f)
        
def drop(u, v):
    """Used for removing the gender direction from the embedding."""
    return u - v * u.dot(v) / v.dot(v)
def v(word, model):
    """ Returns the vector representation of the given word."""
    return model[word]
def get_top_PCA(pairs, model):
    """ Does the PCA on 10 gender pair difference vectors
    and returns the topmost principal component which represents
    the gender direction."""
    matrix = []
    for a, b in pairs:
        center = (v(a, model) + v(b, model))/2
        matrix.append(v(a, model) - center)
        matrix.append(v(b, model) - center)
    matrix = np.array(matrix)
    pca = PCA(n_components = 10)
    pca.fit(matrix)
    return pca.components_[0]  
norms = np.linalg.norm(vecs, axis=1)

if max(norms)-min(norms) > 0.0001:
    normalize(vecs)   

gender_direction = get_top_PCA(definitional_pairs, model)
specific_set = set(tuple(x) for x in gender_specific_words)

for i, w in enumerate(words):
    if w not in specific_set:
        """Removes the gender direction from words which are not gender definitional. """
        vecs[i] = drop(vecs[i], gender_direction)
normalize(vecs) 
def to_utf8(text, errors='strict', encoding='utf8'):
    """Convert a string to bytestring in utf8."""
    if isinstance(text, unicode):
        return text.encode('utf8')    
    return unicode(text, encoding, errors=errors).encode('utf8')
def save_binary_file(filename, binary=True):
    """Saves the  words and vectors into a binary file."""
    with open(filename, 'wb') as fout:
        fout.write(to_utf8("%s %s\n" % vecs.shape))
        for i, word in enumerate(words):
            row = vecs[i]
            if binary:
                fout.write(to_utf8(word) + b" " + row.tobytes())
"""Saves the debiased word embeddings as binary file"""
save_binary_file('bias-in-bio-lab-cssh/word_embeddings/Self_trained_word2vec_debiased.bin')
