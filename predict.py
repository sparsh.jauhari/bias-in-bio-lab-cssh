'''
This file is a runnable file which contains modules for single prediction and bulk prediction on given bios.
'''

from model import svm_predict, load_model
from preprocessing import embedding_transform

def predict_one(X_test, embedding, class_group='medical', sampling='balanced', test_size=0.2, masking=True):
    X_test_embedded = embedding_transform(X_test, embedding, class_group, sampling, test_size, masking)
    svm_trained_model = load_model('svm', embedding, class_group, sampling, test_size, masking)
    prediction = svm_trained_model.predict(X_test_embedded)
    return prediction

def predict_all_models(X_test):

    for embedding in ['cv','self_w2v','w2v','d_w2v']:
        for sampling in ['random','balanced']:
            for masking in [True,False]:
                pred = predict_one(X_test, embedding=embedding, sampling=sampling, masking=masking)

                print("\nembedding :",embedding)
                print("sampling :",sampling)
                print("masking :",masking)
                for i,x in enumerate(X_test):
                    print("sent :",x)
                    print("pred :",pred[i])

    for sampling in ['random','balanced']:
        pred = predict_one(X_test, embedding='elmo', sampling=sampling, masking=True)

        print("\nembedding :",embedding)
        print("sampling :",sampling)
        print("masking :",masking)
        for i,x in enumerate(X_test):
            print("sent :",x)
            print("pred :",pred[i])

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument("--masking", dest='masking',action='store_true', help = "for 'bio' data")
    parser.add_argument("--no-masking", dest='masking',action='store_false', help = "for 'raw' data")

    parser.add_argument("--sampling", help = "choice of sampling / class weights ('random', 'balanced')")
    parser.add_argument("--embedding", help = "choice of embeddings to be used ('cv':count vectorize, 'w2v':word2vec, 'self_w2v': self-trained word2vec, 'elmo':elmo, 'd_w2v:debiased word2vec, 'd_self_w2v': debiased self-trained word2vec)")
    parser.add_argument("--pred_all_models", help = 'yes for all or no for the specific one')

    args = parser.parse_args()
    X_test = ['She works at the hospital','He works at the hospital']
    if args.pred_all_models == 'yes':
        predict_all_models(X_test)
    else:

        pred = predict_one(X_test, embedding=args.embedding, sampling=args.sampling, masking=args.masking)

        print("embedding :",args.embedding)
        print("sampling :",args.sampling)
        print("masking :",args.masking)
        for i,x in enumerate(X_test):
            print("sent :",x)
            print("pred :",pred[i])
