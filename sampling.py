'''
This file contains various data fetching modules.
It can be used to extend the sampling techniques.
'''

from sklearn.model_selection import train_test_split
import pymongo
from config import MONGO_HOST, MONGO_DB, MONGO_COLLECTION, CLASS_GROUP, DATASET_NAMES, SEED
from joblib import dump, load

# Module to fetch data from mongo for a certain class_group
def get_data_from_mongo(class_group):
	print('processing sampling.get_data_from_mongo ...')

	client = pymongo.MongoClient(MONGO_HOST)
	collection = client[MONGO_DB][MONGO_COLLECTION]
	data =  list(collection.find({'$or':[{'title':title} for title in CLASS_GROUP[class_group]]}))

	for i,cursor in enumerate(data):
		data[i]['raw_old'] = data[i]['raw']
		data[i]['raw'] = data[i]['raw_old'][data[i]['start_pos']:]
	
	print("\t saving file : ",DATASET_NAMES['datasets',class_group])
	dump(data, DATASET_NAMES['datasets',class_group]+'.joblib')
	return(data)


# Module to get distinct field values of the given field name
def get_distinct_field_values(field_name):
	print('processing sampling.get_distinct_field_values ...')

	client = pymongo.MongoClient(MONGO_HOST)
	collection = client[MONGO_DB][MONGO_COLLECTION]
	return ([x[field_name] for x in collection.find({},{field_name:1})])


# Module to make new data or load it from the saved dataset
def load_data(class_group, from_saved=True):
	print('processing sampling.load_data ...')

	if not from_saved:
		data = get_data_from_mongo(class_group)
	else:
		data = load(DATASET_NAMES['datasets',class_group]+'.joblib')
	return data


# Module for sample selection
# in our implementation we have provided this functionality but have used class weights for balanced training
def data_selection(data, class_group, sampling, test_size):
	print('processing sampling.data_selection ...')
		
	if sampling == 'random' :
		train,test = train_test_split(data, test_size = test_size, random_state = SEED)
	
	elif sampling == 'balanced':
		# same for now as balancing is done as weights in training
		train,test = train_test_split(data, test_size = test_size, random_state = SEED)
	
	print("\t saving file : ",DATASET_NAMES['datasets',class_group,sampling,test_size])
	dump([train,test], DATASET_NAMES['datasets',class_group,sampling,test_size]+'.joblib')	
	return train,test

