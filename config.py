'''
This file contains all the constants in our projects.
'''

# Mongo Credentials
MONGO_HOST = "mongodb+srv://root:Deployment123@clusterbiobias.4mc8e.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
MONGO_DB = 'biodb'
MONGO_COLLECTION = 'allbio'


# Dataset Constants
TITLE = 'title'
CLASS_GROUP = {'medical' : ['physician',
							'nurse',
							'psychologist',
							'dentist',
							'surgeon',
							'dietitian',	
							'chiropractor'
						],
				'trial' : ['yoga_teacher',
							'personal_trainer']
		}
MASKED = {		True:'bio',
				False:'raw'
		}


# Random Seed used throughout
SEED = 414325


# Paths to downloaded word embeddings
WORD2VEC_PATH = "word_embeddings/GoogleNews-vectors-negative300.bin"
DEBIASED_WORD2VEC_PATH = "word_embeddings/GoogleNews-vectors-negative300-hard-debiased.bin"
DEBIASED_SELF_WORD2VEC_PATH = "word_embeddings/Self_trained_word2vec_debiased.bin"


# File Saving Names for dataset, embeddings and models
# Naming convention : [dset|embd|modl]_[sv]_[cv|wv|tv|ev|dwv|dtv]_[tri|med]_[ran|bal]_[test_spit]_[b|r]
DATASET_NAMES = { 
	
	('embedding','self_w2v','bio') 	: 'word_embeddings/embd__tv____b',
	('embedding','self_w2v','raw') 	: 'word_embeddings/embd__tv____r',

# trial domain
	('datasets','trial') 								: 'datasets/dset___tri___',
	('datasets','trial','random',0.2) 					: 'datasets/dset___tri_ran_0.2_',
	('datasets','trial','balanced',0.2) 				: 'datasets/dset___tri_bal_0.2_',
	
	('embedding','cv','trial','random',0.2,'bio') 		: 'word_embeddings/embd__cv_tri_ran_0.2_b',
	('embedding','cv','trial','random',0.2,'raw') 		: 'word_embeddings/embd__cv_tri_ran_0.2_r',
	('embedding','cv','trial','balanced',0.2,'bio') 	: 'word_embeddings/embd__cv_tri_bal_0.2_b',

	('model','svm','cv','trial','random',0.2,'bio') 	: 'models/modl_sv_cv_tri_ran_0.2_b',
	('model','svm','cv','trial','random',0.2,'raw') 	: 'models/modl_sv_cv_tri_ran_0.2_r',
	('model','svm','w2v','trial','random',0.2,'bio') 	: 'models/modl_sv_wv_tri_ran_0.2_b',
	('model','svm','w2v','trial','random',0.2,'raw') 	: 'models/modl_sv_wv_tri_ran_0.2_r',
	('model','svm','cv','trial','balanced',0.2,'bio') 	: 'models/modl_sv_cv_tri_bal_0.2_b',
	('model','svm','w2v','trial','balanced',0.2,'bio') 	: 'models/modl_sv_wv_tri_bal_0.2_b',
	('model','svm','self_w2v','trial','balanced',0.2,'bio') 	: 'models/modl_sv_tv_tri_bal_0.2_b',
	('model','svm','elmo','trial','balanced',0.2,'bio') 	: 'models/modl_sv_ev_tri_bal_0.2_b',

# medical domain
	('datasets','medical') 								: 'datasets/dset___med___',
	
	('datasets','medical','random',0.2) 				: 'datasets/dset___med_ran_0.2_',
	('datasets','medical','balanced',0.2) 				: 'datasets/dset___med_bal_0.2_',

	('embedding','cv','medical','random',0.2,'bio') 	: 'word_embeddings/embd__cv_med_ran_0.2_b',
	('embedding','cv','medical','random',0.2,'raw') 	: 'word_embeddings/embd__cv_med_ran_0.2_r',
	
	('embedding','cv','medical','balanced',0.2,'bio') 	: 'word_embeddings/embd__cv_med_bal_0.2_b',
	('embedding','cv','medical','balanced',0.2,'raw') 	: 'word_embeddings/embd__cv_med_bal_0.2_r',

	('model','svm','cv','medical','random',0.2,'bio') 	: 'models/modl_sv_cv_med_ran_0.2_b',
	('model','svm','cv','medical','random',0.2,'raw') 	: 'models/modl_sv_cv_med_ran_0.2_r',
	('model','svm','w2v','medical','random',0.2,'bio') 	: 'models/modl_sv_wv_med_ran_0.2_b',
	('model','svm','w2v','medical','random',0.2,'raw') 	: 'models/modl_sv_wv_med_ran_0.2_r',
	('model','svm','self_w2v','medical','random',0.2,'bio') 	: 'models/modl_sv_tv_med_ran_0.2_b',
	('model','svm','self_w2v','medical','random',0.2,'raw') 	: 'models/modl_sv_tv_med_ran_0.2_r',
	('model','svm','elmo','medical','random',0.2,'bio') 	: 'models/modl_sv_ev_med_ran_0.2_b',
	('model','svm','elmo','medical','random',0.2,'raw') 	: 'models/modl_sv_ev_med_ran_0.2_r',
	('model', 'svm', 'd_w2v', 'medical', 'random', 0.2, 'raw') : 'models/modl_sv_dwv_med_ran_0.2_r',
    ('model', 'svm', 'd_w2v', 'medical', 'random', 0.2, 'bio') : 'models/modl_sv_dwv_med_ran_0.2_b',
    ('model', 'svm', 'd_self_w2v', 'medical', 'random', 0.2, 'raw') : 'models/modl_sv_dtv_med_ran_0.2_r',
    ('model', 'svm', 'd_self_w2v', 'medical', 'random', 0.2, 'bio') :'models/modl_sv_dtv_med_ran_0.2_b',
    
	('model','svm','cv','medical','balanced',0.2,'bio') 	: 'models/modl_sv_cv_med_bal_0.2_b',
	('model','svm','cv','medical','balanced',0.2,'raw') 	: 'models/modl_sv_cv_med_bal_0.2_r',
	('model','svm','w2v','medical','balanced',0.2,'bio') 	: 'models/modl_sv_wv_med_bal_0.2_b',
	('model','svm','w2v','medical','balanced',0.2,'raw') 	: 'models/modl_sv_wv_med_bal_0.2_r',
	('model','svm','self_w2v','medical','balanced',0.2,'bio') 	: 'models/modl_sv_tv_med_bal_0.2_b',
	('model','svm','self_w2v','medical','balanced',0.2,'raw') 	: 'models/modl_sv_tv_med_bal_0.2_r',
	('model','svm','elmo','medical','balanced',0.2,'bio') 	: 'models/modl_sv_ev_med_bal_0.2_b',
	('model','svm','elmo','medical','balanced',0.2,'raw') 	: 'models/modl_sv_ev_med_bal_0.2_r',
    ('model','svm','d_self_w2v','medical','balanced',0.2,'raw') 	: 'models/modl_sv_dtv_med_bal_0.2_r',
    ('model','svm','d_self_w2v','medical','balanced',0.2,'bio') 	: 'models/modl_sv_dtv_med_bal_0.2_b',
    ('model','svm','d_w2v','medical','balanced',0.2,'bio') 	: 'models/modl_sv_dwv_med_bal_0.2_b',
	('model','svm','d_w2v','medical','balanced',0.2,'raw') 	: 'models/modl_sv_dwv_med_bal_0.2_r'   
}


# file saving names for prediction set
# Naming convention : pred_[sv]_[cv|wv|tv|ev|dwv|dtv]_[tri|med]_[ran|bal]_[test_spit]_[b|r]
PREDICTED_DATASET = {
	
	('svm','cv','trial','balanced',0.2,'raw') 	: 'predicted_datasets/pred_sv_cv_tri_bal_0.2_r',
	('svm','cv','trial','balanced',0.2,'bio') 	: 'predicted_datasets/pred_sv_cv_tri_bal_0.2_b',
	('svm','w2v','trial','balanced',0.2,'raw') 	: 'predicted_datasets/pred_sv_wv_tri_bal_0.2_r',
	('svm','w2v','trial','balanced',0.2,'bio') 	: 'predicted_datasets/pred_sv_wv_tri_bal_0.2_b',
	('svm','self_w2v','trial','balanced',0.2,'raw') 	: 'predicted_datasets/pred_sv_tv_tri_bal_0.2_r',
	('svm','self_w2v','trial','balanced',0.2,'bio') 	: 'predicted_datasets/pred_sv_tv_tri_bal_0.2_b',
	('svm','elmo','trial','balanced',0.2,'raw') 	: 'predicted_datasets/pred_sv_ev_tri_bal_0.2_r',
	('svm','elmo','trial','balanced',0.2,'bio') 	: 'predicted_datasets/pred_sv_ev_tri_bal_0.2_b',
	
	('svm','cv','trial','random',0.2,'raw') 	: 'predicted_datasets/pred_sv_cv_tri_ran_0.2_r',
	('svm','cv','trial','random',0.2,'bio') 	: 'predicted_datasets/pred_sv_cv_tri_ran_0.2_b',
	('svm','w2v','trial','random',0.2,'raw') 	: 'predicted_datasets/pred_sv_wv_tri_ran_0.2_r',
	('svm','w2v','trial','random',0.2,'bio') 	: 'predicted_datasets/pred_sv_wv_tri_ran_0.2_b',
	('svm','self_w2v','trial','random',0.2,'raw') 	: 'predicted_datasets/pred_sv_tv_tri_ran_0.2_r',
	('svm','self_w2v','trial','random',0.2,'bio') 	: 'predicted_datasets/pred_sv_tv_tri_ran_0.2_b',
	('svm','elmo','trial','random',0.2,'raw') 	: 'predicted_datasets/pred_sv_ev_tri_ran_0.2_r',
	('svm','elmo','trial','random',0.2,'bio') 	: 'predicted_datasets/pred_sv_ev_tri_ran_0.2_b',

	('svm','cv','medical','random',0.2,'raw') 	: 'predicted_datasets/pred_sv_cv_med_ran_0.2_r',
	('svm','cv','medical','random',0.2,'bio') 	: 'predicted_datasets/pred_sv_cv_med_ran_0.2_b',
	('svm','w2v','medical','random',0.2,'raw') 	: 'predicted_datasets/pred_sv_wv_med_ran_0.2_r',
	('svm','w2v','medical','random',0.2,'bio') 	: 'predicted_datasets/pred_sv_wv_med_ran_0.2_b',
	('svm','self_w2v','medical','random',0.2,'raw') 	: 'predicted_datasets/pred_sv_tv_med_ran_0.2_r',
	('svm','self_w2v','medical','random',0.2,'bio') 	: 'predicted_datasets/pred_sv_tv_med_ran_0.2_b',
    ('svm','d_w2v','medical','random',0.2,'raw') 	: 'predicted_datasets/pred_sv_dwv_med_ran_0.2_r',
	('svm','d_w2v','medical','random',0.2,'bio') 	: 'predicted_datasets/pred_sv_dwv_med_ran_0.2_b',
	('svm','d_self_w2v','medical','random',0.2,'raw') 	: 'predicted_datasets/pred_sv_dtv_med_ran_0.2_r',
	('svm','d_self_w2v','medical','random',0.2,'bio') 	: 'predicted_datasets/pred_sv_dtv_med_ran_0.2_b',
	('svm','elmo','medical','random',0.2,'raw') 	: 'predicted_datasets/pred_sv_ev_med_ran_0.2_r',
	('svm','elmo','medical','random',0.2,'bio') 	: 'predicted_datasets/pred_sv_ev_med_ran_0.2_b',
	
	('svm','cv','medical','balanced',0.2,'raw') 	: 'predicted_datasets/pred_sv_cv_med_bal_0.2_r',
	('svm','cv','medical','balanced',0.2,'bio') 	: 'predicted_datasets/pred_sv_cv_med_bal_0.2_b',
	('svm','w2v','medical','balanced',0.2,'raw') 	: 'predicted_datasets/pred_sv_wv_med_bal_0.2_r',
	('svm','w2v','medical','balanced',0.2,'bio') 	: 'predicted_datasets/pred_sv_wv_med_bal_0.2_b',
    ('svm','d_w2v','medical','balanced',0.2,'raw') 	: 'predicted_datasets/pred_sv_dwv_med_bal_0.2_r',
	('svm','d_w2v','medical','balanced',0.2,'bio') 	: 'predicted_datasets/pred_sv_dwv_med_bal_0.2_b',
	('svm','self_w2v','medical','balanced',0.2,'raw') 	: 'predicted_datasets/pred_sv_tv_med_bal_0.2_r',
	('svm','self_w2v','medical','balanced',0.2,'bio') 	: 'predicted_datasets/pred_sv_tv_med_bal_0.2_b',
    ('svm','d_self_w2v','medical','balanced',0.2,'raw') 	: 'predicted_datasets/pred_sv_dtv_med_bal_0.2_r',
	('svm','d_self_w2v','medical','balanced',0.2,'bio') 	: 'predicted_datasets/pred_sv_dtv_med_bal_0.2_b',
	('svm','elmo','medical','balanced',0.2,'raw') 	: 'predicted_datasets/pred_sv_ev_med_bal_0.2_r',
	('svm','elmo','medical','balanced',0.2,'bio') 	: 'predicted_datasets/pred_sv_ev_med_bal_0.2_b'
}
