'''
This file contained the model definitions and related training and predicting commands.
It provides the space and tools to expand our code to include more classification models.
'''

from sklearn.svm import SVC
from config import DATASET_NAMES, MASKED, SEED
from joblib import dump, load
import numpy as np



# generic module for model training
def model_training(X_train, Y_train, model, embedding, class_group, sampling, test_size, masking):
	print("processing model.model_training ...")
	if model == 'svm':
		trained_model = svm_train(X_train, Y_train, sampling)
	print("\t saving file :",DATASET_NAMES['model',model,embedding,class_group,sampling,test_size,MASKED[masking]])
	dump(trained_model, DATASET_NAMES['model',model,embedding,class_group,sampling,test_size,MASKED[masking]]+'.joblib')



# generic module for predictions on a given set
def model_prediction(X_test, Y_test, model, embedding, class_group, sampling, test_size, masking):
	print("processing model.model_prediction ...")
	trained_model = load(DATASET_NAMES['model',model,embedding,class_group,sampling,test_size,MASKED[masking]]+'.joblib')
	if model == 'svm':
		pred, acc = svm_predict(X_test, Y_test, trained_model)
	return pred, acc



# Load trained model from saved file
def load_model(model, embedding, class_group, sampling, test_size, masking):
	return(load(DATASET_NAMES['model',model,embedding,class_group,sampling,test_size,MASKED[masking]]+'.joblib'))



# Support Vector Machine
def svm_train(X_train, Y_train, sampling):

	if sampling == 'balanced':
		class_weight = 'balanced'
	elif sampling == 'random':
		class_weight = None

	classifier = SVC(C=1, kernel = 'linear', gamma = 'auto', class_weight=class_weight,random_state=SEED)
	classifier.fit(X_train, Y_train)
	return classifier


def svm_predict(X_test, Y_test, classifier):
	prediction = classifier.predict(X_test)
	acc = np.mean(prediction == Y_test)
	return prediction, acc
